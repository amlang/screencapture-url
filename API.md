## Classes

<dl>
<dt><a href="#ScreenCapture">ScreenCapture</a></dt>
<dd></dd>
</dl>

<a name="ScreenCapture"></a>

## ScreenCapture
**Kind**: global class

* [ScreenCapture](#ScreenCapture)
    * [new ScreenCapture([url], [options])](#new_ScreenCapture_new)
    * [.from(uri)](#ScreenCapture+from) ⇒
    * [.of(uri)](#ScreenCapture+of) ⇒
    * [.to(_path)](#ScreenCapture+to) ⇒
    * [.withName(name)](#ScreenCapture+withName) ⇒
    * [.withDimensions(width, height)](#ScreenCapture+withDimensions) ⇒
    * [.width(width)](#ScreenCapture+width) ⇒
    * [.height(height)](#ScreenCapture+height) ⇒
    * [.fromPosition(left, top)](#ScreenCapture+fromPosition) ⇒
    * [.left(x)](#ScreenCapture+left) ⇒
    * [.top(y)](#ScreenCapture+top) ⇒
    * [.clip(clipping)](#ScreenCapture+clip) ⇒
    * [.theWholePage()](#ScreenCapture+theWholePage) ⇒
    * [.fullpage()](#ScreenCapture+fullpage) ⇒
    * [.asJpg()](#ScreenCapture+asJpg) ⇒
    * [.asJpeg()](#ScreenCapture+asJpeg) ⇒
    * [.withQuality(quality)](#ScreenCapture+withQuality) ⇒
    * [.asPng()](#ScreenCapture+asPng) ⇒
    * [.encode(encoding)](#ScreenCapture+encode) ⇒
    * [.asBinary()](#ScreenCapture+asBinary) ⇒
    * [.asBase64String()](#ScreenCapture+asBase64String) ⇒
    * [.asBase64File(filepath)](#ScreenCapture+asBase64File) ⇒
    * [.andSaveAsText()](#ScreenCapture+andSaveAsText) ⇒
    * [.as(type)](#ScreenCapture+as) ⇒
    * [.buildPath()](#ScreenCapture+buildPath) ⇒
    * [.take([url])](#ScreenCapture+take) ⇒

<a name="new_ScreenCapture_new"></a>

### new ScreenCapture([url], [options])
ScreenCapture()


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [url] | <code>String</code> \| <code>Url</code> | <code>&#x27;null&#x27;</code> | Url to capture |
| [options] | <code>Object</code> | <code>{}</code> |  |

**Example**
```
// fast example
(async () =>
   const result = await new Screenshot()
       .of('http://example.com')
       .to('.')
       .withName('my_screenshot')
       .asPng()
       .take()
   console.log(result) // Output: { path: './my_screenshot.png', screenshot: <Buffer 89 50 4e 47 0d 0a ...>  }
)()
```
<a name="ScreenCapture+from"></a>

### screenCapture.from(uri) ⇒
Alias for `of()`

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: of()

| Param | Type |
| --- | --- |
| uri | <code>String</code> \| <code>Url</code> |

**Example**
```
 new ScreenCapture().from('http://example.com')
```
<a name="ScreenCapture+of"></a>

### screenCapture.of(uri) ⇒
Sets the url to screencapture

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

| Param | Type |
| --- | --- |
| uri | <code>String</code> \| <code>Url</code> |

**Example**
```
 new ScreenCapture().of('http://example.com')
```
<a name="ScreenCapture+to"></a>

### screenCapture.to(_path) ⇒
Sets the directory or file path to where the image should be saved

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**Throws**:
- SystemError ENOENT


| Param | Type |
| --- | --- |
| _path | <code>String</code> |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('/path/to/save') // or .to('.')
// or
 new ScreenCapture()
   .of('http://example.com')
   .to('/path/to/screenshot.png') // sets path and screenshot identifier
```
<a name="ScreenCapture+withName"></a>

### screenCapture.withName(name) ⇒
Sets the identifier of the image
- default is an uuidv4 String

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

| Param | Type |
| --- | --- |
| name | <code>String</code> |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .withName('my_screenshot.png')
```
<a name="ScreenCapture+withDimensions"></a>

### screenCapture.withDimensions(width, height) ⇒
Sets the height and width of the clipping path
- shortcut for `.width(..).height(..)`
- default is 1024px to 768px @see constructor

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: width(), height()

| Param | Type |
| --- | --- |
| width | <code>Number</code> |
| height | <code>Number</code> |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .withName('my_screenshot.png')
   .withDimensions(100, 100) // width and height in pixels
```
<a name="ScreenCapture+width"></a>

### screenCapture.width(width) ⇒
Sets the width of the clipping path
- default is 1024px @see constructor

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**Throws**:
- AssertionError if width <= 0

**See**: withDimensions(), height()

| Param | Type |
| --- | --- |
| width | <code>Number</code> |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .width(100) // in pixels
```
<a name="ScreenCapture+height"></a>

### screenCapture.height(height) ⇒
Sets the height of the clipping path
- default is 768px @see constructor

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**Throws**:
- AssertionError if height <= 0

**See**: withDimensions(), width(), clip()

| Param | Type |
| --- | --- |
| height | <code>Number</code> |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .width(100)
   .height(100) // in pixels
```
<a name="ScreenCapture+fromPosition"></a>

### screenCapture.fromPosition(left, top) ⇒
Sets the left and top (or x and y) position of the clipping path
- shortcut for `.left(..).top(..)`
- default is 0 to 0 @see constructor

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: left(), top(), clip()

| Param | Type |
| --- | --- |
| left | <code>Number</code> |
| top | <code>Number</code> |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .withDimensions(100, 100)
   .fromPosition(0,0)  // Take screenshot of React(0, 0, 100, 100)
```
<a name="ScreenCapture+left"></a>

### screenCapture.left(x) ⇒
Sets the left or x positition of the clipping path
- default is 0 @see constructor

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**Throws**:
- AssertionError if x is negative

**See**: fromPosition(), top(), clip()

| Param | Type |
| --- | --- |
| x | <code>Number</code> |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .withDimensions(100, 100)
    // would take a screenshot from 100px left to 200px left
   .left(100)  // left start position of screenshot area / clipping path
```
<a name="ScreenCapture+top"></a>

### screenCapture.top(y) ⇒
Sets the top postition or y position of the clipping path

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**Throws**:
- AssertionError if y is negative

**See**: fromPosition(), left(), clip()

| Param | Type |
| --- | --- |
| y | <code>Number</code> |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .withDimensions(100, 100)
    // would take a screenshot from 100px left to 200px left
   .left(100)  // left start position of screenshot area / clipping path
    // would take a screenshot from 250px top to 350px top
   .top(250)  // top start position of screenshot area / clipping path
```
<a name="ScreenCapture+clip"></a>

### screenCapture.clip(clipping) ⇒
Sets the clipping path

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: fromPosition(), left(), top(), width(), height()

| Param | Type | Description |
| --- | --- | --- |
| clipping | <code>Object</code> \| <code>Array</code> | e.g. {[x||left]:0, [y||top]:0, width: 1024, height: 768} or as array [0, 0, 1024, 768] |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   // named clipping path
   .clip({x: 100, y: 100, width: 100, height: 100})
   // clipping path as array [left, top, width, height]
   .clip([100, 100, 100, 100])
```
<a name="ScreenCapture+theWholePage"></a>

### screenCapture.theWholePage() ⇒
Alias for `fullpage()` (ignores a previously set clipping!)

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: fullpage(), clip()

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .theWholePage() // e.g. Screenshot of 768px width and 5500px height
```
<a name="ScreenCapture+fullpage"></a>

### screenCapture.fullpage() ⇒
Sets a option to screencapture the whole webpage (ignores a previously set clipping!)

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: clip()

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .fullpage() // e.g. Screenshot of 768px width and 5500px height
```
<a name="ScreenCapture+asJpg"></a>

### screenCapture.asJpg() ⇒
Sets the output format to JPEG - shortcut for `as('JPG')`

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this
TODO add quality parameter

**See**: as()

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .asJpg() // take() would returns a jpeg screenshot
```
<a name="ScreenCapture+asJpeg"></a>

### screenCapture.asJpeg() ⇒
Sets the output format to JPEG - shortcut for `as('JPEG')`

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this
TODO add quality parameter

**See**: as()

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .asJpeg() // take() would returns a jpeg screenshot
```
<a name="ScreenCapture+withQuality"></a>

### screenCapture.withQuality(quality) ⇒
Sets the quality of the image
- has no effect if png is selected as output format

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**Throws**:

- AssertionError if options.type is not JPEG
- AssertionError if quality is not between 0 and 100


| Param | Type |
| --- | --- |
| quality | <code>Number</code> |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .asJpg()
   .withQuality(50)
```
<a name="ScreenCapture+asPng"></a>

### screenCapture.asPng() ⇒
Sets the output format to PNG - shortcut for `as('PNG')`

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: as()

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .asPng() // take() would returns a png screenshot
```
<a name="ScreenCapture+encode"></a>

### screenCapture.encode(encoding) ⇒
Sets the output encoding
   'binary' - Image would be returned as Binary Stream
   'base64' - Image would be returned as Base64 String

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**Throws**:
- AssertionError if encoding is not one of 'binary' or 'base64'


| Param | Type |
| --- | --- |
| encoding | <code>&#x27;binary&#x27;</code> \| <code>&#x27;base64&#x27;</code> |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .asPng()
   .encode('base64') // take() would returns a base64 screenshot
   // or
   .encode('binary') // take() would returns a Buffer screenshot
```
<a name="ScreenCapture+asBinary"></a>

### screenCapture.asBinary() ⇒
Sets the output encoding to 'binary'

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: encode()

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .asBinary() // take() would returns a Buffer screenshot
```
<a name="ScreenCapture+asBase64String"></a>

### screenCapture.asBase64String() ⇒
Sets the output encoding to 'base64'

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: encode()

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .asBase64String() // take() would returns a base64 screenshot
```
<a name="ScreenCapture+asBase64File"></a>

### screenCapture.asBase64File(filepath) ⇒
Sets the output encoding to 'base64' and saves string as file

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: encode(), andSaveAsText()

| Param | Type | Description |
| --- | --- | --- |
| filepath | <code>String</code> | where the text file is to be saved |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .asBase64File() // take() would returns a base64 screenshot / saves screenshot as textfile
```
<a name="ScreenCapture+andSaveAsText"></a>

### screenCapture.andSaveAsText() ⇒
Sets the option to save image as text file

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: encode(), buildPath()

<a name="ScreenCapture+as"></a>

### screenCapture.as(type) ⇒
Sets the output formats and output encodings

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

**See**: encode(), asJpg(), asJpeg(), asPng(), asBinary(), asBase64File(), asBase64String()

| Param | Type | Description |
| --- | --- | --- |
| type | <code>String</code> | one of 'binary', 'base64', 'jpg', 'jpeg' or 'png' |

**Example**
```
 new ScreenCapture()
   .of('http://example.com')
   .to('.')
   .as('binary') // take() would returns a Buffer screenshot / saves screenshot as imagefile
   .as('base64') // take() would returns a base64 screenshot
   .as('jpg') // take() saves screenshot as jpeg file
   .as('jpeg') // take() saves screenshot as jpeg file
   .as('png') // take() saves screenshot as png file
```
<a name="ScreenCapture+buildPath"></a>

### screenCapture.buildPath() ⇒
Composes the path of the image to be saved

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: @this

<a name="ScreenCapture+take"></a>

### screenCapture.take([url]) ⇒
Captures and saves the screenshot
```
// Short example "How to screencapture http://example.com"
new Screenshot('http://example.com')
   .take()
   .then(( res ) => console.log(res))     // Output: { path: './865ebf8b-d141-483b-a2df-eb70d5a7d05c.png', screenshot: <Buffer 89 50 4e 47 0d 0a ...>  }

// or
new Screenshot('http://example.com')
   .take()
   .then(( {path} ) => console.log(path)) // Output: './865ebf8b-d141-483b-a2df-eb70d5a7d05c.png'

// or with async / await
(async () =>
   const result = await new Screenshot('http://example.com').take()
   console.log(result) // Output: { path: './865ebf8b-d141-483b-a2df-eb70d5a7d05c.png', screenshot: <Buffer 89 50 4e 47 0d 0a ...>  }
)()
```

**Kind**: instance method of [<code>ScreenCapture</code>](#ScreenCapture)

**Returns**: Object with the path where the screenshot could be found and the screenshot itself as Stream or String

| Param | Type | Description |
| --- | --- | --- |
| [url] | <code>String</code> | optional Instead of setting it in the constructor or using the .of() / .from() method |
