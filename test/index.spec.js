/**
 * Copyright (c) 2018 Alexander Lang <alexander.m.lang@googlemail.com>
 *
 * Licensed under the MIT License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://choosealicense.com/licenses/mit
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * @author Alexander Lang <alexander.m.lang@googlemail.com>
 * @copyright 2018 Alexander Lang <alexander.m.lang@googlemail.com>
 * @license MIT https://choosealicense.com/licenses/mit
 */
'use strict';
const t = require('tap'); // eslint-disable-line import/newline-after-import
t.mochaGlobals();
const {describe, it} = require('tap/lib/mocha');
const should = require('should');
const {imgDiff} = require('img-diff-js');
const url = require('url');
const path = require('path');
const {ScreenCapture} = require('..');

const {stringify} = JSON;

const OUR_TEST_URL = 'http://localhost:8080/';
const OUR_DEFAULT = {
  url: null,
  saveAsText: false,
  options: {
    clip: {
      x: 0,
      y: 0,
      width: 1024,
      height: 768
    },
    type: 'png',
    encoding: 'binary',
    launch: {}
    // Path: '/var/folders/07/gf60ws1n7pz1jdyjgfpcw8200xxrr3/T',
    // name: '9b9e2695-be33-4358-ae97-c922d34b4b77'
  }
};

describe('ScreenCapture', () => {
  describe('constructor()', () => {
    it('should match the default', () => {
      const s = new ScreenCapture();
      // If (os.platform === 'darwin') {
      delete s.options.path; // Random nonreal tmp path on macOS
      // }
      delete s.options.name; // Uuidv4 string
      stringify(s).should.be.equal(stringify(OUR_DEFAULT));
    });

    it(`should set url to ${OUR_TEST_URL}, if set in constructor`, () => {
      const s = new ScreenCapture(OUR_TEST_URL);
      s.url.should.equal(OUR_TEST_URL);
    });

    it(`should set name to 'abcd.some.ext', if set in constructor`, () => {
      const s = new ScreenCapture(OUR_TEST_URL, {name: 'abcd.some.ext'});
      s.options.name.should.equal('abcd.some.ext');
    });
  });

  describe('from()', () => {
    it(`should set the URL option to ${OUR_TEST_URL} if the parameter is of type String`, () => {
      const s = new ScreenCapture().from(OUR_TEST_URL);
      s.url.should.equal(OUR_TEST_URL);
    });

    it(`should set the URL option to ${OUR_TEST_URL} if the parameter is of type URL`, () => {
      const uri = url.parse(OUR_TEST_URL);
      const s = new ScreenCapture().from(uri);
      s.url.should.equal(OUR_TEST_URL);
    });
  });

  describe('of()', () => {
    it(`should set the URL option to ${OUR_TEST_URL} if the parameter is of type String`, () => {
      const s = new ScreenCapture().of(OUR_TEST_URL);
      s.url.should.equal(OUR_TEST_URL);
    });

    it(`should set the URL option to ${OUR_TEST_URL} if the parameter is of type URL`, () => {
      const uri = url.parse(OUR_TEST_URL);
      const s = new ScreenCapture().of(uri);
      s.url.should.equal(OUR_TEST_URL);
    });
  });

  describe('to()', () => {
    it('should set path to resolved ./ and name to uuid', () => {
      const p = './';
      const s = new ScreenCapture()
        .of(OUR_TEST_URL)
        .to(p);
      // UUID match
      s.options.name.should.match(/[\w]{8}(-[\w]{4}){3}-[\w]{12}/);
      s.options.path.should.equal(path.resolve(path.dirname(p)));
    });

    it('should set path to resolved ./ and name to a_filename.1', () => {
      const p = './a_filename.1';
      const s = new ScreenCapture()
        .of(OUR_TEST_URL)
        .to(p);
      s.options.name.should.equal(path.basename(path.resolve(p)));
      s.options.path.should.equal(path.dirname(path.resolve(p)));
    });

    it('should set path to resolved ./ and name to a_filename.1', () => {
      const p = 'a_filename.1';
      const s = new ScreenCapture()
        .of(OUR_TEST_URL)
        .to(p);
      s.options.name.should.equal(path.basename(path.resolve(p)));
      s.options.path.should.equal(path.dirname(path.resolve(p)));
    });
  });

  describe('withName()', () => {
    it('should set name to a_filename.1', () => {
      const s = new ScreenCapture().withName('a_filename.1');
      s.options.name.should.equal('a_filename.1');
    });

    it('should set name to uuid, if name is null', () => {
      const s = new ScreenCapture().withName(null);
      // UUID match
      s.options.name.should.match(/[\w]{8}(-[\w]{4}){3}-[\w]{12}/);
    });
  });

  describe('withDimensions()', () => {
    it('should set width and height to 800 x 600', () => {
      const s = new ScreenCapture().withDimensions(800, 600);
      s.options.clip.width.should.equal(800);
      s.options.clip.height.should.equal(600);
    });
  });

  describe('width()', () => {
    it('should set width to 666', () => {
      const s = new ScreenCapture().width(666);
      s.options.clip.width.should.equal(666);
      s.options.clip.height.should.equal(768); // Default
    });

    it('should raise error if parameter is lower than or equal to 0', () => {
      should.throws(() => new ScreenCapture().width(-1), 'The width MUST be greater than 0');
    });
  });

  describe('height()', () => {
    it('should set height to 666', () => {
      const s = new ScreenCapture().height(666);
      s.options.clip.width.should.equal(1024); // Default
      s.options.clip.height.should.equal(666);
    });

    it('should raise error if parameter is lower than or equal to 0', () => {
      should.throws(() => new ScreenCapture().height(-1), 'The height MUST be greater than 0');
    });
  });

  describe('fromPosition()', () => {
    it('should set screencapture position to P(100|100)', () => {
      const s = new ScreenCapture().fromPosition(100, 100);
      s.options.clip.x.should.equal(100);
      s.options.clip.y.should.equal(100);
    });
  });

  describe('left()', () => {
    it('should only set x coordinate of clipping position P(100|0)', () => {
      const s = new ScreenCapture().left(100);
      s.options.clip.x.should.equal(100);
      s.options.clip.y.should.equal(0); // Default
    });

    it('should raise error if parameter is lower than 0', () => {
      should.throws(() => new ScreenCapture().left(-1), 'The x coordinate MUST be greater than or equal 0.');
    });
  });

  describe('top()', () => {
    it('should only set y coordinate of clipping position P(0|100)', () => {
      const s = new ScreenCapture().top(100);
      s.options.clip.x.should.equal(0); // Default
      s.options.clip.y.should.equal(100);
    });

    it('should raise error if parameter is lower than 0', () => {
      should.throws(() => new ScreenCapture().top(-1), 'The y coordinate MUST be greater than or equal 0.');
    });
  });

  describe('clip()', () => {
    it('should set clipping position (P(100|100)) and area (100 x 100) for screenshot, if parameter is a React', () => {
      const s = new ScreenCapture().clip({x: 100, y: 100, width: 100, height: 100});
      s.options.clip.x.should.equal(100);
      s.options.clip.y.should.equal(100);
      s.options.clip.width.should.equal(100);
      s.options.clip.height.should.equal(100);
    });

    it('should set clipping position (P(100|100)) and area (100 x 100) for screenshot, if parameter is a object', () => {
      const s = new ScreenCapture().clip({left: 100, top: 100, width: 100, height: 100});
      s.options.clip.x.should.equal(100);
      s.options.clip.y.should.equal(100);
      s.options.clip.width.should.equal(100);
      s.options.clip.height.should.equal(100);
    });

    it('should set clipping position (P(100|100)) and area (100 x 100) for screenshot, if parameter is a array', () => {
      const s = new ScreenCapture().clip([100, 100, 100, 100]);
      s.options.clip.x.should.equal(100);
      s.options.clip.y.should.equal(100);
      s.options.clip.width.should.equal(100);
      s.options.clip.height.should.equal(100);
    });
  });

  describe('theWholePage()', () => {
    it('should set the fullpage option', () => {
      const s = new ScreenCapture().theWholePage();
      s.options.fullPage.should.equal(true);
    });
  });

  describe('fullpage()', () => {
    it('should set the fullpage option', () => {
      const s = new ScreenCapture().fullpage();
      s.options.fullPage.should.equal(true);
    });
  });

  describe('asJpg()', () => {
    it('should set type to jpeg', () => {
      const s = new ScreenCapture().asJpg();
      s.options.type.should.equal('jpeg');
    });
  });

  describe('asJpeg()', () => {
    it('should set type to jpeg', () => {
      const s = new ScreenCapture().asJpeg();
      s.options.type.should.equal('jpeg');
    });
  });

  describe('asPng()', () => {
    it('should set type to jpeg', () => {
      const s = new ScreenCapture().asPng();
      s.options.type.should.equal('png');
    });
  });

  describe('withQuality()', () => {
    it('should set quality to 50', () => {
      const s = new ScreenCapture().asJpg().withQuality(50);
      s.options.quality.should.equal(50);
    });

    it('should raise error if type is not jpeg', () => {
      should.throws(() => new ScreenCapture().asPng().withQuality(50), 'Type MUST be jpeg!');
    });

    it('should raise error if quality is lower than 0', () => {
      should.throws(() => new ScreenCapture().asJpeg().withQuality(-1), 'Type MUST be greater than or equal to 0!');
    });

    it('should raise error if quality is greater than 100', () => {
      should.throws(() => new ScreenCapture().asJpeg().withQuality(101), 'Type MUST be less than or equal to 100!');
    });
  });

  describe('encode()', () => {
    it('should have a default binary encoding', () => {
      const s = new ScreenCapture();
      s.options.encoding.should.equal('binary');
    });

    it('should set encoding to base64, case insensitive', () => {
      const s = new ScreenCapture().encode('BASE64');
      s.options.encoding.should.equal('base64');
    });

    it('should raise if encoding is not one of binary or base64', () => {
      should.throws(() => new ScreenCapture().encode('asd'), `The encoding MUST be one of 'binary' or 'base64'.`);
    });
  });

  describe('asBinary()', () => {
    it('should set encoding to binary', () => {
      const s = new ScreenCapture().asBinary();
      s.options.encoding.should.equal('binary');
    });
  });

  describe('asBase64String()', () => {
    it('should set encoding to base64', () => {
      const s = new ScreenCapture().asBase64String();
      s.options.encoding.should.equal('base64');
    });
  });

  describe('asBase64File()', () => {
    it('should set encoding to base64, saveAsFile to true and filepath to ./a_filepath.1', () => {
      const p = './a_filepath.1';
      const s = new ScreenCapture().asBase64File(p);
      s.options.encoding.should.equal('base64');
      s.saveAsText.should.equal(true);
      s.options.name.should.equal(path.basename(path.resolve(p)));
      s.options.path.should.equal(path.dirname(path.resolve(p)));
    });
  });

  describe('andSaveAsText()', () => {
    it('should set saveAsText to true', () => {
      const s = new ScreenCapture().andSaveAsText();
      s.saveAsText.should.equal(true);
    });
  });

  describe('as()', () => {
    it('should set encoding to base64', () => {
      const s = new ScreenCapture().as('base64');
      s.options.encoding.should.equal('base64');
    });

    it('should set encoding to jpeg, if parameter is jpg', () => {
      const s = new ScreenCapture().as('jpg');
      s.options.type.should.equal('jpeg');
    });

    it('should set encoding to png', () => {
      const s = new ScreenCapture().as('png');
      s.options.type.should.equal('png');
    });
  });

  describe('buildPath()', () => {
    it('should create correct path with .png extension', () => {
      const p = './a_filepath.jpeg';
      const s = new ScreenCapture().to(p);
      s.buildPath().should.equal(path.resolve(p) + '.png');
    });

    it('should create correct path with .jpeg extension', () => {
      const p = './a_filepath.jpeg';
      const s = new ScreenCapture().asJpeg().to(p);
      s.buildPath().should.equal(path.resolve(p) + '.jpeg');
    });

    it('should create correct path with .txt extension', () => {
      const p = './a_filepath.an_extension';
      const s = new ScreenCapture().asBase64File(p);
      s.buildPath().should.equal(path.resolve(p) + '.txt');
    });
  });

  describe('take()', () => {
    const launch = {
      executablePath: process.env.CHROME_BIN || undefined,
      ignoreHTTPSErrors: true,
      args: ['--no-sandbox', '--headless', '--disable-gpu', '--disable-dev-shm-usage']
    };

    it('should raise an error', async () => {
      const s = new ScreenCapture(null, {launch});
      s.take().should.be.rejectedWith({message: 'Protocol error (Page.navigate): Invalid parameters url: string value expected'});
    });

    it('should create an screenshot of testpage - url via constructor', async () => {
      const s = new ScreenCapture(OUR_TEST_URL, {launch});
      const result = await s.to(path.resolve(path.join(__dirname, 'assets/result_1_1024_x_768'))).take();
      const diff = await imgDiff({
        actualFilename: path.resolve(path.join(__dirname, 'assets/expected_1024_x_768.png')),
        expectedFilename: result.path,
        diffFilename: path.resolve(path.join(__dirname, 'assets/diff_1.png'))
      });
      // ? console.log(diff);
      diff.imagesAreSame.should.be.true();
    });

    it('should create an screenshot of testpage - url via take method', async () => {
      const s = new ScreenCapture(null, {launch});
      const result = await s.to(path.resolve(path.join(__dirname, 'assets/result_2_1024_x_768'))).take(OUR_TEST_URL);
      const diff = await imgDiff({
        actualFilename: path.resolve(path.join(__dirname, 'assets/expected_1024_x_768.png')),
        expectedFilename: result.path,
        diffFilename: path.resolve(path.join(__dirname, 'assets/diff_2.png'))
      });
      // ? console.log(diff);
      diff.imagesAreSame.should.be.true();
    });

    it('should create an screenshot of testpage - fullsize', async () => {
      const s = new ScreenCapture(OUR_TEST_URL, {launch});
      const result = await s.to(path.resolve(path.join(__dirname, 'assets/result_fullscreen'))).fullpage().take();
      const diff = await imgDiff({
        actualFilename: path.resolve(path.join(__dirname, 'assets/expected_fullpage.png')),
        expectedFilename: result.path,
        diffFilename: path.resolve(path.join(__dirname, 'assets/diff_3.png'))
      });
      // ? console.log(diff);
      diff.imagesAreSame.should.be.true();
    });
  });
});
